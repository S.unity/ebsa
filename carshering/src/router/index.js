import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/HelloWorld'
import services from '../components/v-services'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/services/:id',
    name: 'services',
    component: services,
    props: true
  }
]

const router = new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})

export default router
